---
title: Web Development
description: General information about the technologies used for web development
published: 1
date: 2020-02-27T12:00:30.352Z
tags: 
editor: undefined
dateCreated: 2020-02-27T12:00:22.001Z
---

# Content Types

## .ndjson

[Find the specification here](https://github.com/ndjson/ndjson-spec).

ndjson or "Newline delimited JSON" is used for optimisations purposes.
It chunks the data into lines, allowing streams reading to be faster.

[Gitlab talks about it here](https://gitlab.com/gitlab-org/gitlab/issues/197171).