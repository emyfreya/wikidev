---
title: Unit testing
description: 
published: true
date: 2022-03-02T13:03:42.802Z
tags: java
editor: markdown
dateCreated: 2022-03-02T09:56:14.250Z
---

# Integration test

On the test class, add the following attributes :

```java
@SpringBootTest
@AutoConfigureMockMvc
```

`MockMvc` is a http client helper for unit testing.
Declare a field as followed :

```java
@Autowired
MockMvc mockMvc;
```

Using JPA and repositories, to test with the database, it is necessary that it is transactional.
To make the test class transactional, `extends` from `AbstractTransactionalJUnit4SpringContextTests`.

If any dependency is necessary, add a field with the dependency and the attribute `@Autowired`.

# MockMvc

## Assert json

```java
.andExpect(jsonPath("$.myProperty").value(3))
```

## Deserialize the response

Using `com.fasterxml.jackson.core` :

```java
public static <T> T deserialize(ObjectMapper mapper, MvcResult mvcResult)
		throws JsonMappingException, JsonProcessingException, UnsupportedEncodingException
{
	String json = mvcResult.getResponse().getContentAsString();

	return mapper
			.readValue(
					json,
					new TypeReference<T>()
					{});
}
```