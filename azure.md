---
title: Azure
description: 
published: 1
date: 2021-02-04T16:19:57.112Z
tags: 
editor: undefined
dateCreated: 2020-03-09T15:54:01.913Z
---

# Log Analytics

A solution for your applications to log into a database like system, meaning queryable. 

# API Managment

## Policy

To write policies, we have to use the `xml` markup language. It supports .Net Frameworks functionnalities.

### Exemple
Here's an exemple of a condition.
```xml
<choose>
		<when condition="@((string)context.Variables["status"] == "403")">
				<return-response>
						<set-status code="403" reason="Forbidden" />
						<set-body>
								<value>@("|Error="+context.Variables["status"])</value>
						</set-body>
				</return-response>
		</when>
</choose>
```

We can access the context, even play with `JObject` types.

### Sources
* [XML Syntax](https://docs.microsoft.com/en-us/azure/api-management/api-management-policy-expressions)

## Caching
The caching is managed by the service, so we can store and get the values.

### Sources

* [Caching policies](https://docs.microsoft.com/en-us/azure/api-management/api-management-caching-policies)