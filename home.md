---
title: 🧙‍♀️ Welcome to Emy's dev wiki ✨
description: All of my developer experience in one place.
published: true
date: 2024-02-18T02:46:14.562Z
tags: 
editor: markdown
dateCreated: 2020-02-25T11:35:49.019Z
---

# Welcome!

Here I save every bit of knowledge that I met up along the path of my career.

This wiki is available [here](https://gitlab.com/emyfreya/wikidev) in Markdown files.

# About me

[My Website](https://emysramen.net/)
[Gitlab (Most active)](https://gitlab.com/emyfreya)
[Github](https://github.com/emyfreya)

