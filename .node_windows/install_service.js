var Service = require('node-windows').Service;
var base = require('./service.js');

// Create a new service object
var svc = new Service({
  name: base.serviceName,
  description: 'My Local Wiki',
  script: base.scriptPath,
  workingDirectory: 'C:\\Wiki\\',
  scriptOptions: 'server',
  nodeOptions: [
    '--harmony',
    '--max_old_space_size=4096'
  ]
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
  svc.start();
  console.log(svc);
});

svc.install();