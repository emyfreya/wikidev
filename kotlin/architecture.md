---
title: Architecture
description: 
published: true
date: 2022-03-16T13:05:03.086Z
tags: 
editor: markdown
dateCreated: 2022-03-15T08:29:19.698Z
---

# Clean architecture

## Sources

- [Clean architecture for Android with Kotlin: a pragmatic approach for starters](https://antonioleiva.com/clean-architecture-android/)

## Conception

The different layers are :
  
```plantuml
<style>
activityDiagram {
  BackgroundColor #1e1e1e
  FontColor #FFF
  BorderColor #FFF
}
</style>
:**DB**
Repository, database;
:**Use cases**
Classes that corresponds to different actions to react to;
:**Framework**
Activity, Fragments;
```