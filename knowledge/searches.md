---
title: Searches
description: 
published: 1
date: 2020-02-25T11:46:12.902Z
tags: 
editor: undefined
dateCreated: 2020-02-25T11:46:11.398Z
---

# Azure Api Managment

https://docs.microsoft.com/en-us/azure/api-management/api-management-access-restriction-policies#ValidateJWT
https://docs.microsoft.com/en-us/azure/api-management/api-management-caching-policies
https://docs.microsoft.com/en-us/azure/api-management/policies/authorize-request-using-external-authorizer
https://docs.microsoft.com/en-us/azure/api-management/set-edit-policies

## About Jwt and Gateways

https://medium.com/@robert.broeckelmann/how-to-submit-your-security-tokens-to-an-api-provider-pt-1-4a68df35843a
https://medium.com/@robert.broeckelmann/how-to-submit-your-security-tokens-to-an-api-provider-pt-2-26cb85cfc97a
https://www.imranaftabrana.com/2019/01/windows-oauth-openid-and-third-party.html

## Jwt in NetCore

http://jasonwatmore.com/post/2018/08/14/aspnet-core-21-jwt-authentication-tutorial-with-example-api
https://www.meziantou.net/2018/04/30/jwt-authentication-with-asp-net-core

## Custom way to check the Jwt validity ?

https://salslab.com/a/jwt-authentication-and-authorisation-in-asp-net-core-web-api
https://stackoverflow.com/questions/29048122/token-based-authentication-in-asp-net-core/29698502#29698502
