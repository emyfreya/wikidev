---
title: Prism
description: 
published: 1
date: 2020-02-25T11:47:00.598Z
tags: 
editor: undefined
dateCreated: 2020-02-25T11:46:59.226Z
---

# Navigation Failed Suspending App
## Error
Happens when an `object` is passed through the navigation system (with `INavigationService`).

```
Exception thrown: 'System.Runtime.InteropServices.COMException' in Prism.Windows.dll
Unspecified error

GetNavigationState doesn't support serialization of a parameter type which was passed to Frame.Navigate.`
```

## Solution

Re-implemented the `ISessionStateService` to catch and log the exception.
While using the `INavigationService.Navigate(token, parameter)` interface's method, the developper must pass a parameter which is a simple type (or primitive type). i.e. `string`, `int`, `Guid`, and so on.