---
title: Thirdparty
description: 
published: 1
date: 2020-02-25T11:46:22.243Z
tags: 
editor: undefined
dateCreated: 2020-02-25T11:46:20.728Z
---

|  Library  |  Description  |  Repositories  |  Websites  |  Documentation |
|---|---|---|---|---|
|  [AutoMapper](thirdparty/automapper)  |   Map an object to another object |   |   |  http://docs.automapper.org/en/stable/ |
|  AngleSharp  |  Manipulate HTML and DOM documents  |  https://github.com/AngleSharp/AngleSharp  |   |   |
|  [CommandLineUtils](thirdparty/commandlineutils)  |  Create a command line app  |  https://github.com/natemcmaster/CommandLineUtils |   |   |
| Hangfire | | | https://www.hangfire.io/ | |
| [LiteDb](thirdparty/litedb) | NoSQL storage from local  |  |  http://www.litedb.org/ | |
| [NetCore.AutoRegisterDi](thirdparty/netcoreautoregisterdi) |  Register all classes with their interfaces to the `IServiceProvider` which are in any Assembly | https://github.com/JonPSmith/NetCore.AutoRegisterDi | | |
| Serilog |  Logs everything to any 'sink' that could exists (file, azure, output, etc...) | https://github.com/serilog/serilog |  |   |
| [ColorThieft](thirdparty/colorthieft) | Get the palette or the dominant color from an image (uri). | https://github.com/KSemenenko/ColorThief | | |