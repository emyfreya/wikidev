---
title: Sqlite
description: 
published: 1
date: 2020-02-25T11:46:19.878Z
tags: 
editor: undefined
dateCreated: 2020-02-25T11:46:18.418Z
---

# List foreign keys
```sql
PRAGMA foreign_key_list(my_table)
```

# List indexes
```sql
PRAGMA index_list(my_table)
```