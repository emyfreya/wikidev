---
title: Automapper
description: 
published: 1
date: 2020-02-25T11:46:47.399Z
tags: 
editor: undefined
dateCreated: 2020-02-25T11:46:45.942Z
---

# Creating maps
Create a class as simple as this

```csharp
using AutoMapper;

public class MyMapsProfile : Profile
{
	public MyMapsProfile()
	{
		this.CreateMap<Source, Destination>() ...
	}
}
```

# Adding maps

With `IServiceCollection`, we can add it like this

```csharp
// Use Assembly.Load if it is in another assembly.
services.AddAutoMapper(Assembly.GetExecutingAssembly());
```