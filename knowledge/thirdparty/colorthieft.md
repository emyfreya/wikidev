---
title: Colorthieft
description: 
published: 1
date: 2020-02-25T11:46:49.632Z
tags: 
editor: undefined
dateCreated: 2020-02-25T11:46:48.217Z
---

<!-- SUBTITLE: A quick summary of Colorthieft -->

# UWP
To use it in UWP, copy-paste the following code:

```csharp
using Windows.Graphics.Imaging;
using Windows.Storage.Streams;
using ColorThiefDotNet;

// Code.

var random = RandomAccessStreamReference.CreateFromUri(newValue);
using (IRandomAccessStream randomAccessStream = await random.OpenReadAsync())
{
    var decoder = await BitmapDecoder.CreateAsync(randomAccessStream);

    var colorThief = new ColorThief();
    QuantizedColor quantizedColor = await colorThief.GetColor(decoder);

    return new Windows.UI.Color()
    {
        A = quantizedColor.Color.A,
        B = quantizedColor.Color.B,
        G = quantizedColor.Color.G,
        R = quantizedColor.Color.R
    };
}
```