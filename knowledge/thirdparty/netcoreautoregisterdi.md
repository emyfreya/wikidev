---
title: NetCore.AutoRegisterDi
description: 
published: 1
date: 2020-02-25T11:46:54.085Z
tags: 
editor: undefined
dateCreated: 2020-02-25T11:46:52.705Z
---

# Registering classes
As simple as 3 lines of codes:

```csharp
services.RegisterAssemblyPublicNonGenericClasses(Assembly.Load("My.Assembly"))
	// Only those classes.
	.Where(c => c.Name.EndsWith("Repository"))
	// Register the Interfaces.
	.AsPublicImplementedInterfaces();
```

Where `services` is `Microsoft.Extensions.DependencyInjection.IServiceCollection`.