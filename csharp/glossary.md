---
title: Glossary
description: Words to live by
published: 1
date: 2020-09-15T13:05:56.716Z
tags: 
editor: undefined
dateCreated: 2020-09-15T07:40:08.500Z
---

# TaskCancellationSource
# Semaphore
# SynchronizationContext
# BlockingCollection
# Work Item (Threading)
# Task
# Thread
# Operation
# OperationCancelledException
# TaskCancelledException
# ExclusiveScheduler
# ConcurrentScheduler
# AsyncLazy (Class)