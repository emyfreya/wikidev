---
title: Project & Package references
description: 
published: true
date: 2022-12-07T08:45:59.398Z
tags: 
editor: markdown
dateCreated: 2021-12-08T10:44:53.387Z
---

# Avoid flow of a package

At some point, we want the packages (NuGet packages) that are used in our project to not be used by a another project that references it.

To avoid the flow of a nuget package, in the `.csproj`, configure the wanted package like so with `PrivateAssets="compile"` :

```xml 
<PackageReference Include="My.Package" Version="1.0.1" PrivateAssets="compile" />
```

> Note that `PrivateAssets="all"` will tell the compiler to not include the library at all, which will create a runtime exception.
{.is-warning}

# Internal method reference from another project

For example, unit testing, it could be necessary to access method with the `internal` protection level.

To do so, in the `.csproj`, add the following lines :

```xml
  <ItemGroup>
    <AssemblyAttribute Include="System.Runtime.CompilerServices.InternalsVisibleTo">
      <_Parameter1>MyService.Tests</_Parameter1>
    </AssemblyAttribute>
  </ItemGroup>
```

or the newer version from `.NET 5` :

```xml
<ItemGroup>
  <InternalsVisibleTo Include="MyService.Tests" />
</ItemGroup>
```

# Central package managment

Manage packages in a centralized place, by using the fact that `Directory.Packages.props` is taking automatically into account.

- [Introducing Central Package Management](https://devblogs.microsoft.com/nuget/introducing-central-package-management/)
- [Package Source Mapping](https://learn.microsoft.com/en-gb/nuget/consume-packages/package-source-mapping)