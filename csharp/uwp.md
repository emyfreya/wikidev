---
title: Universal Windows Plateform
description: Everything about UWP.
published: 1
date: 2021-05-11T07:46:03.478Z
tags: c#, uwp
editor: markdown
dateCreated: 2020-02-27T15:18:30.800Z
---

# ToggleSwitch

## IsOn

First thing to obverse are the bindings going away when the user clicks it.
Solutions are to get the binding expression on the `ToggleSwitch` and set it back when the `Toggled` events is invoked.

## Touch is not working but the click is

If the click event is raised but not the touch event, it is because the touch event is intercepted by another source.

In the component's `Template` you're using, search for a `Presenter` and set the property `ManipulationMode` to `None`: `ManipulationMode="None"`.

# Xaml exceptions

## Failed to assign to property 'Windows.UI.Xaml.ResourceDictionary.Source'

```
Failed to assign to property 'Windows.UI.Xaml.ResourceDictionary.Source' because the type 'Windows.Foundation.String' cannot be assigned to the type 'Windows.Foundation.Uri'.
```

Was due to:

```xml
<!-- wrong link, Something.xaml does not exists. -->
<ResourceDictionary Source="ms-appx:///myapp.app/Something.xaml" />
```

## InvalidCastException on a binding

This exception

```csharp
Exception thrown: 'System.InvalidCastException' in App.exe
Unable to cast object of type 'Windows.UI.Xaml.Data.Binding' to type 'Windows.UI.Xaml.Visibility'.
```

happens when the property on the used componen **Isn't** a **DependencyProperty**.

# Getting any component Style from the current Windows 10 SDK
To get any `Style` from the base components of UWP, the usual path is:

`C:\Program Files (x86)\Windows Kits\10\DesignTime\CommonConfiguration\Neutral\UAP\10.0.17134.0\Generic`

`10.0.17134.0` is the SDK version.

# Data triggers

Here's an example:

```xml
<Page
xmlns:Core="using:Microsoft.Xaml.Interactions.Core"
xmlns:Interactivity="using:Microsoft.Xaml.Interactivity">

<ContentControl>
	<Interactivity:Interaction.Behaviors>
		<Core:DataTriggerBehavior Binding="{Binding MyBinding}" Value="0">
				<Core:ChangePropertyAction PropertyName="Background" Value="White" />
		</Core:DataTriggerBehavior>
		<Core:DataTriggerBehavior Binding="{Binding MyBinding}" Value="1">
				<Core:ChangePropertyAction PropertyName="Background" Value="Green" />
		</Core:DataTriggerBehavior>
		<Core:DataTriggerBehavior Binding="{Binding MyBinding}" Value="2">
				<Core:ChangePropertyAction PropertyName="Background" Value="Red" />
		</Core:DataTriggerBehavior>
	</Interactivity:Interaction.Behaviors>
</ContentControl>

</Page>
```

# Bindings

## Binding an element outside of the VisualTree

> Needs more research
{.is-warning}


In the XAML, create an event handler from the **Loaded** event:

```csharp
<Button
  Loaded="Button_Loaded"
  />
```

In the **Loaded** event, we're gonna reset the binding source via an extension method:

```csharp
public Button_Loaded(object sender, RoutedEventArgs e)
{
		var button = sender as FrameworkElement;

		button.UpdateBindingSource(
				dependencyProperty: Button.CommandProperty,
				source: this);
}
```

The extension method `UpdateBindingSource` can [be found here](https://gitlab.com/-/snippets/2034939)

## Bind an enum

> This could've a been done my rendering enum nullable.
{.is-info}

The command must have a type `object` as a parameter, then convert it:

```csharp
public DelegateCommand<object> MyCommand 
{
	get => _MyCommand ?? (_MyCommand = new DelegateCommand<object>(
		(payload) => 
		{
			MyEnum @enum = (MyEnum)payload;
		});
}
```

In the XAML:

```xml
<Button Command="{Binding MyCommand}">
	<Button.CommandParameter>
			<xmlnsNamespace:MyEnum>Test</xmlnsNamespace:MyEnum>
	</Button.CommandParameter>
</Button>
```

