---
title: Dependency Injection
description: 
published: 1
date: 2020-09-15T12:50:07.418Z
tags: 
editor: undefined
dateCreated: 2020-02-28T10:10:52.746Z
---

# Different types of containers

* Microsoft.Extensions.DependencyInjection
	Mostly used in **ASP .NET Core** apps
* Unity
* AutoFac

# Scopes

## Singleton

The same instance is returned in the whole application.

## Scoped

A new instance out of the scope of the request, or the same instance if in the scope.

### Example

Considering `IDependency` as **Scoped** service.

```csharp
public class Service
{
  // Dependency resolved in the scope of the service
  public Service(IDependency dependency)
  {
    ...
  }
}

public class AnotherService
{
  // Dependency resolved in the scope of the service
  public AnotherService(IDependency dependency)
  {
    ...
  }
}

// Resolving both Controllers in different scopes

public class Controller
{
  // The same instance of IDependency is given to AnotherService and Service.
  public Controller(IDependency dependency, AnotherService anotherService, Service service)
  {
    ...
  }
}

public class AnotherController
{
  // The same instance of IDependency is given to AnotherService and Service.
  // Note: IDependency will be another instance, because the scope has changed
  public AnotherController(IDependency dependency, AnotherService anotherService, Service service)
  {
    ...
  }
}
```

## Transient

A new instance everytime the type is requested.

## Injection constructors should be simple.

[Injection Constructors should be simple by Mark Seemann](https://blog.ploeh.dk/2011/03/03/InjectionConstructorsshouldbesimple/)

The only logic there is, is the assignment of dependencies. No additional logic should be done.
It's to keep the concept of layers as best as possible, making our APIs light by making loosely coupled code.