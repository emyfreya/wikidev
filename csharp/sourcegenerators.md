---
title: Source Generators
description: .NET 5 newly introduced source generators
published: true
date: 2022-10-03T08:55:03.400Z
tags: 
editor: markdown
dateCreated: 2021-11-17T12:51:27.337Z
---

# What

Generates code at the time of editing files, allowing the usage of code not yet compiled.

# Why

Source generators are mainly here to optimize the use of reflection in C# code.

# How

As the name implies, source generators (or incremental generators) generates code, in a separate file such as this file ends by `.g.cs`.
The name is decided by the developer of the library.

Here's a sample of [it here](https://gitlab.com/RamenTurismo/net-incremental-generators/-/blob/main/src/RamenTurismo.Generators/ToStringGenerator.cs).

The sample generates the `ToString` method of a class that fullfill those 2 conditions : It is `partial` and has `ToStringAttribute`.