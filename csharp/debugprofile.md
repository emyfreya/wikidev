---
title: Debugging / Profiling
description: 
published: true
date: 2022-09-23T08:11:34.050Z
tags: 
editor: markdown
dateCreated: 2021-04-27T08:54:36.177Z
---

# .NET Profiling

[Measure app performance in Visual Studio](https://docs.microsoft.com/en-us/visualstudio/profiling/?WT.mc_id=vstoolbox-c9-niner%2F%3Fview%3Dvs-2019&view=vs-2019)

# .NET Async Profiling

[Analyze performance of .NET asynchronous code](https://docs.microsoft.com/en-us/visualstudio/profiling/analyze-async?WT.mc_id=vstoolbox-c9-niner&view=vs-2019)

# Breakpoints

## Data breakpoint

Breaks when a data changes.

# Intellitrace

## Snapshots

Allows to take snapshots of exceptions. A snapshot is a point in time where the code was executed, allowing to replay the scenario. 

![intellitrace_snapshot_configuration.png](/images/intellitrace_snapshot_configuration.png)

# dotnet counters

> Only works for .Net Core / .NET 5+ frameworks.  
{.is-warning}

Monitors the GC, ThreadPool, ASP .NET Core hosting requests.

```
System.Runtime
    cpu-usage                                    The percent of process' CPU usage relative to all of the system CPU resources [0-100]
    working-set                                  Amount of working set used by the process (MB)
    gc-heap-size                                 Total heap size reported by the GC (MB)
    gen-0-gc-count                               Number of Gen 0 GCs between update intervals
    gen-1-gc-count                               Number of Gen 1 GCs between update intervals
    gen-2-gc-count                               Number of Gen 2 GCs between update intervals
    time-in-gc                                   % time in GC since the last GC
    gen-0-size                                   Gen 0 Heap Size
    gen-1-size                                   Gen 1 Heap Size
    gen-2-size                                   Gen 2 Heap Size
    loh-size                                     LOH Size
    alloc-rate                                   Number of bytes allocated in the managed heap between update intervals
    assembly-count                               Number of Assemblies Loaded
    exception-count                              Number of Exceptions / sec
    threadpool-thread-count                      Number of ThreadPool Threads
    monitor-lock-contention-count                Number of times there were contention when trying to take the monitor lock between update intervals
    threadpool-queue-length                      ThreadPool Work Items Queue Length
    threadpool-completed-items-count             ThreadPool Completed Work Items Count
    active-timer-count                           Number of timers that are currently active

Microsoft.AspNetCore.Hosting
    requests-per-second                  Number of requests between update intervals
    total-requests                       Total number of requests
    current-requests                     Current number of requests
    failed-requests                      Failed number of requests
```

## Install

Via [powershell core](/powershell#installing-powershell-core) :

```
dotnet tool install --global dotnet-counters 
```

## How to

Type the following command to find the process id (pid) :

```
dotnet-counters ps
```

Then this command to monitor :

```
dotnet-counters monitor -p 1234
```

# dotnet dump

* [Microsoft Doc](https://docs.microsoft.com/en-us/dotnet/core/diagnostics/dotnet-dump)

## Install

```
dotnet tool install --global dotnet-dump
```

## How to

First get the process ID :

```
dotnet dump ps
```

Then create a `.dmp` :
```
dotnet dump collect -p <pid>
```

Once the `.dmp` has been created, you can analyze it with :

```
dotnet dump analyze <path>
```

## Analyze mode

We could analyze the state of the ThreadPool at the moment of the dump via the `threadpool` command :

```
PS C:\> dotnet dump analyze C:\Path\dump_20210602_113550.dmp
Loading core dump: C:\Path\dump_20210602_113550.dmp ...
Ready to process analysis commands. Type 'help' to list available commands or 'help [command]' to get detailed help on a command.
Type 'quit' or 'exit' to exit the session.
> threadpool
CPU utilization: 20 %
Worker Thread: Total: 20 Running: 7 Idle: 0 MaxLimit: 20 MinLimit: 1
Work Request in Queue: 0
--------------------------------------
Number of Timers: 0
--------------------------------------
Completion Port Thread:Total: 12 Free: 12 MaxFree: 16 CurrentLimit: 12 MaxLimit: 1000 MinLimit: 1
```
