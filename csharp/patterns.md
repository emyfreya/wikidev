---
title: Patterns
description: An overview on patterns that used with C#
published: true
date: 2021-11-29T13:58:16.266Z
tags: 
editor: markdown
dateCreated: 2020-03-18T15:45:37.212Z
---

# MVVM

## View

Everything that needs to be shown to the user. Commonly used files is `.xaml`.

## ViewModel

Classes that needs to hold informations for the View. It will never know what's the view is about: It just holds informations. 
It can also manage events such as Commands that are in fact, click on a button.

## Model

The service layer. It gets the data or whatever you need to fill the ViewModel.

# Strategy

## Sources

* https://www.dofactory.com/net/strategy-design-pattern

# Unit testing: AAA

A : Arrange

Build up the necessary things to make the unit test.

A : Act

Call methods that needs to be asserted.

A : Assert

Check the method outcome.

# Factory

## Lazy

Lazy loading can be implemented in a thread safe manner with `Lazy<T>` or `AsyncLazy<T>`.

The async manner has been implemented in [AsyncEx](https://github.com/StephenCleary/AsyncEx).