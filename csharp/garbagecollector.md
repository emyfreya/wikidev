---
title: Garbage Collector (GC)
description: 
published: 1
date: 2021-05-17T08:28:12.915Z
tags: 
editor: markdown
dateCreated: 2020-07-22T12:28:26.874Z
---

# Sources

[The Hunger Games of .NET Garbage Collection](https://medium.com/@anandgupta.08/the-hunger-games-of-net-garbage-collection-cb45cd74fb0b)

# Eager Root Collection

// TODO

# DPAD on GC

* [Put a DPAD on that GC!](https://devblogs.microsoft.com/dotnet/put-a-dpad-on-that-gc/)

# Debugging a Memory Leak

The command [dotnet-trace](https://docs.microsoft.com/en-us/dotnet/core/diagnostics/dotnet-trace) outputs the current memory usage of an application.