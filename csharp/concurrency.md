---
title: Concurrency
description: How to manage different types of concurrency in C#
published: true
date: 2021-11-17T12:41:07.754Z
tags: 
editor: markdown
dateCreated: 2021-02-09T10:03:33.747Z
---

# Channel

`Channel<T>` is a queue to manage async concurrency with an observer pattern (consumer / producer).

## Sources

* [An Introduction to Channels in C#](https://jeremybytes.blogspot.com/2021/02/an-introduction-to-channels-in-c.html)

# Concurrent Queue

Manage a multi-thread queue in a thread safe manner.

## Sources

* [What's the Difference between Channel and ConcurrentQueue in C#?](https://jeremybytes.blogspot.com/2021/02/whats-difference-between-channel-and.html)