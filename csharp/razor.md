---
title: Blazor & Razor
description: 
published: true
date: 2021-11-18T21:14:41.336Z
tags: razor, blazor
editor: markdown
dateCreated: 2021-11-17T19:55:18.967Z
---

# Components

## View components

A view component allows to isolate a part of the UI as a single model.
Typicaly, a View as its own ViewModel, it's the same for a View Component.

### Support

- Dependency injection 
- Passing parameters
- Multiple `View()` calls. For example, there is a `Default.cshtml`, then a `NotDefault.cshtml`, where each could be shown depending on data obtained through DI.

## Razor components

A razor component is used with Blazor services. Those services are added through the use of a Blazor Server or a Blazor Web Assembly (Client Side UI).

## Which one to use ?

[This blog article](https://andrewlock.net/dont-replace-your-view-components-with-razor-components/) explains it well : Razor components are a way to do some lonely UI sided component, not really to generate server side HTML.

# Localization routing 

Configure the middleware 

```csharp
app.UseRequestLocalization(options =>
{
    CultureInfo[] supportedCultures = new[]
    {
       new CultureInfo("en-US"),
       new CultureInfo("fr-FR"),
       new CultureInfo("jp-JP")
    };

    options.RequestCultureProviders.Clear();

    options.RequestCultureProviders.Add(new RouteDataRequestCultureProvider());
    options.RequestCultureProviders.Add(new QueryStringRequestCultureProvider());
    options.RequestCultureProviders.Add(new CookieRequestCultureProvider());
    options.RequestCultureProviders.Add(new AcceptLanguageHeaderRequestCultureProvider());

    options.SupportedCultures = supportedCultures;
    options.SupportedUICultures = supportedCultures;
    options.DefaultRequestCulture = new RequestCulture(supportedCultures[0], supportedCultures[0]);
});
```

Add a new Razor convention `IPageRouteModelConvention`.
This changes the template of every page discovered at startup, making the first element of the path the culture.

```csharp
public class RouteDataRouteModelConvention : IPageRouteModelConvention
{
    public void Apply(PageRouteModel model)
    {
        List<SelectorModel> selectorModels = new();
        
        foreach (SelectorModel selector in model.Selectors)
        {
            string? template = selector.AttributeRouteModel?.Template;

            if(template == null)
            {
                continue;
            }

            selectorModels.Add(new SelectorModel()
            {
                AttributeRouteModel = new AttributeRouteModel
                {
                    Template = $"/{{culture}}/{template}"
                }
            });
        }

        foreach (SelectorModel m in selectorModels)
        {
            model.Selectors.Add(m);
        }
    }
}
```

Then add it to razor conventions in services :

```csharp
builder.Services.AddRazorPages()
    .AddRazorPagesOptions(configure =>
    {
        configure.Conventions.Add(new RouteDataRouteModelConvention());
    });
```