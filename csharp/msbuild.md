---
title: MSBuild
description: Explanation about msbuild
published: 1
date: 2020-11-12T09:53:54.497Z
tags: 
editor: undefined
dateCreated: 2020-10-15T09:24:29.374Z
---

# Concept

msbuild executes multiples tasks. Those tasks are represented in a .csproj (for example) by the `<Target` attribute.

# Customize a build

[Customize your build](https://docs.microsoft.com/en-us/visualstudio/msbuild/customize-your-build?view=vs-2019)

## Choose between Directory.Build.props and Directory.Build.targets

The instructions to how your build behaves will be in this.
If put in the repository's root, it will be taken into account by `msbuild.exe`.

# Declare a property

To declare a property, the `PropertyGroup` is needed.

```xml
<PropertyGroup>
  <NugetName>$(TargetName.ToLower())</NugetName>
</PropertyGroup>
```

`$(TargetName)` is a msbuild common property which is available at build.
You can find the [rest here](https://docs.microsoft.com/en-us/cpp/build/reference/common-macros-for-build-commands-and-properties?view=msvc-160).

# UsingTask and Task

`UsingTask` is simply a class that'll be use in the build process.
It can have Input and Output properties and contains C# code.

An example here, we want to get a directory full path using Linq.

`NugetRootPath` represents a property used in the execution method (`<Code ...`) and is mandatory (`Required="true"`).
`NugetVersionPath` reprents a property used in an output (`<Output`).

```xml
<UsingTask TaskName="GetVersionDirectory"
            TaskFactory="RoslynCodeTaskFactory"
            AssemblyFile="$(MSBuildToolsPath)\Microsoft.Build.Tasks.Core.dll">
  <ParameterGroup>
    <NugetRootPath ParameterType="System.String" Required="true" />
    <NugetVersionPath ParameterType="System.String" Output="true" />
  </ParameterGroup>
  <Task>
    <Using Namespace="System" />
    <Using Namespace="System.Linq" />
    <Code Type="Fragment" Language="cs">
      <![CDATA[
        var directories = Directory.GetDirectories(
          NugetRootPath,
          "*",
          System.IO.SearchOption.TopDirectoryOnly);
        
        NugetVersionPath = directories.OrderByDescending(e => e).First();
        Log.LogMessage(MessageImportance.High, $" InCache version path determined: {NugetVersionPath}");
      ]]>
    </Code>
  </Task>
</UsingTask>
```

To use this task inside a `Target`:

```xml
<Target Name="GetNugetCacheDirectory" AfterTargets="AfterBuild" Condition="'$(Configuration)' == 'Release' AND Exists($(NugetCacheVersionDirectory))">
  <GetVersionDirectory NugetRootPath="$(MyVarDeclaredOnAPropertyGroup)">
    <Output PropertyName="NugetVersionPathOutput" TaskParameter="NugetVersionPath" />
  </GetVersionDirectory>
  <Message Importance="High" Text="$(NugetVersionPathOutput)" />
</Target>
```

# Targets

[Default MSBuild targets](https://docs.microsoft.com/en-us/visualstudio/msbuild/msbuild-targets?view=vs-2019)
[A good list of available targets](https://gist.github.com/StevenLiekens/cae70cce25344ba47b86)

# Execution

A runable executable can be found on the computer at:
`C:\Program Files (x86)\Microsoft Visual Studio\2019\Enterprise\MSBuild\Current\Bin\msbuild.exe`

## Arguments for: UWP

For Windows store apps, msbuild takes into account the concept of packaging.

`/p:AppxBundlePlatforms="x64"`
`/p:AppxPackageDir="C:\\AppxPackages\\"` 
`/p:AppxBundle=Always`
`/p:platform="Any CPU"`
`/p:configuration="Release"`

## Basic arguments

`/p:VisualStudioVersion="16.0"`
`/p:DefineConstants="Release"`

## Packaging

MSBuild will build the app in `bin\$(Platform)\$(Configuration)` then copies all the libraries from each project's `Release` folder `bin` to the app's `obj\$(Platform)\$(Configuration)\PackageLayout` directory.

# Copy

## Copy a folder's structure to another

To copy a folder while keeping the file and subfolders, use `%(RecursiveDir)` in the `DestinationFolder`.

```xml
<Target Name="CopyToNuGetCache" AfterTargets="GetNugetCacheDirectory" Condition="Exists($(MyPath))">
  <ItemGroup>
    <MyFolderAndFiles Include="$(MySource)\**\*.*" />
  </ItemGroup>
  <Copy SourceFiles="@(MyFolderAndFiles)" DestinationFolder="$(MyPath)\%(RecursiveDir)" SkipUnchangedFiles="true" />
  <Message Importance="High" Text=" Copied $(MySource) to $(MyPath)" />
</Target>
```