---
title: Report of my Async Programming Course from Dotnetos Academy
description: https://academy.dotnetos.org/
published: true
date: 2024-02-18T02:47:35.702Z
tags: 
editor: markdown
dateCreated: 2020-06-19T08:48:39.612Z
---

Git project associated with [this repository](https://gitlab.com/emyfreya/async-expert-course).

# Summary

- [Threading basics](threadbasics/)
  Explains the basics of threading
- [Task](tasks/)
  Usage of promises in modern C#
- [Async Await](asyncawait/)
- [ValueTask](valuetask/)
- [Execution, contexts, scheduler](contexts/)
- [Sync vs Async](syncxasync/)
- [Fire & forget](fireforget/)
- [TaskCompletionSource](taskcompletionsource/)
- [Awaitables](awaitables/)
- [Locals](locals/)
- [Tasks aggregations](tasksaggregations/)
- [IAsyncDisposable](iasyncdisposable/)
- [IAsyncEnumerable](iasyncenumerable/)
- [Low Level Concurrency](lowlevelconcurrency/)
- [volatile](volatile/)
- [Interlocked](interlocked/)
- [lock](lock/)
- [Async primitives](asyncprimitives/)
- [Concurrent data structures](/csharp/async/course/concurrentdatastructures)
- [New Concurrent data structures](/csharp/async/course/newconcurrentdatastructures)
- [Miscellaneous](/csharp/async/course/miscellaneous)

# BenchmarkDotNet

[Github](https://github.com/dotnet/BenchmarkDotNet)

- OpenSource
- NuGet package to inclue in a Console app
- Compatible NetCore 3.1
- Simple to setup: Via attribute or option pattern

# SharpLab.io

Converts the current code (i.e. CSharp) to IL, JIT Assembly, and more.
Allows the developer to see what the language hides behind the scene and to optimize the produced code.

## Features

Examples with C#.

`Inspect.Allocation(Action)` allows to see how much allocation is taken on the heap by the ran method.

# SuperBenchmarker

To do some load tests on endpoints.

[Source](https://github.com/aliostad/SuperBenchmarker)

# Concurrency visualizer

[Diagnostic Concurrency Visualizer](https://marketplace.visualstudio.com/items?itemName=Diagnostics.DiagnosticsConcurrencyVisualizer2019) is a Visual Studio extension that enables to see via graphics the cpu usage, what's happening in each thread via a trace.

It can come useful to see what's causing the performance issues on some parts of the code, because it can offer the capability to show how many times the thread has been interrupted or when the operation switched cores.

[About Concurrency Visualizer](https://docs.microsoft.com/pl-pl/visualstudio/profiling/concurrency-visualizer?view=vs-2019)