---
title: Concurrent Data Structures
description: 
published: true
date: 2022-02-15T16:21:55.103Z
tags: 
editor: markdown
dateCreated: 2022-02-15T14:31:00.826Z
---

# Foundations

2 sets of operations :

- Read, reads a snapshot of a value
- Write, changes the data in the data structure

For data structures that are used to communicate between workers :

- Producers : Put values into the queue
- Consumers : Take out values from the queue

There can be :

- Multiple workers on a collection  (Handles multiple consumers / producers)
- A single worker, for example single producer / consumer

## Categories

- SPSC : Single producer, Single consumer
- MPSC : Multi-producer, Single consumer
- SPMC : Single Producer, Multi consumer
- MPMC : Multi Producer, Multi consumer

Volatile : Emitting half berriers, eventually consistent state
Interlocked : Emitting full barriers, atomic operations using Read-Modify-Write

# BlockingCollection

It is a Thread safe wrap around any `IProducerConsumerCollection<T>`.
It supports `CancellationToken`.

It can be enumerated via `GetConsumingEnumerable` :
It will yield items that will be set as consumed.
Different operations can get differents items. A single item will be always be consumed only once.

No `async` support.

## Initialization

By default, it uses FIFO semantics.
We can also pass another collection into the constructor to use LIFO (`ConcurrentStack`).

# ConcurrentStack

A thread safe LIFO, lock free adding.
Has a `PushRange` method to have better performance in a loop scenario.

A stack is a chain of nodes.
It allocates a node on very push to do the swap.

# Concurrent Queue

A queue is just a list.

It uses an atomic reservation for an item to be dequeue, such as using `Volatile.Read` and `Volatile.Write`.

## Count

Count returns a snapshot of the queue at a specific moment. It might be incorrect as soon as it's accessed.

## Enumeration

Same as Count, a snapshot is made from the queue to return an enumerator of this snapshot.
It uses a yield return.

# Concurrent dictionary

A snapshot is made from the queue to return an enumerator of this snapshot.

Those operations uses `Interlocked` semantics :

- TryUpdate (Similar to `Interlocked.CompareExchange`)
- GetOrAdd (With factory or `TValue` overload)

> Factory code is called outside the lock !
> It also may be called multiple times
{.warning}

To go around the problem of the Factory that may called multiple times, we could use `AsyncLazy<T>`.

Is contains a locking array (An array of `object`), which is changed everytime an operation is called.
It is resized when the dictionary is resized.
It can be tweaked via the constructor `(int concurrencyLevel, int capacity)`.
A single lock object is used for multiple an operation that is done in parallel.

Reads are lock-free.
