---
title: ValueTask, ValueTask<T>
description: 
published: 1
date: 2020-08-13T09:04:03.582Z
tags: 
editor: undefined
dateCreated: 2020-08-13T07:20:09.879Z
---

# Sources

- [Understanding the Whys, Whats, and Whens of ValueTask](https://devblogs.microsoft.com/dotnet/understanding-the-whys-whats-and-whens-of-valuetask/)
- [Prefer ValueTask to Task, always; and don't await twice](https://blog.marcgravell.com/2019/08/prefer-valuetask-to-task-always-and.html)

# Problem

The **state machine** is used even when the operation ends synchronously.

## Example

```csharp
private async Task<int> CountAsync(int from)
{
  if (from < 2)
  {
    return 0;
  }
  
  return await CountAsync(from);
}
```

# First solution

We can get rid of the state machine allocation by returning a Task directly, which will improve performance.

```csharp
private Task<int> CountAsync(int from)
{
  if (from < 2)
  {
    return Task.FromResult(0);
  }
  
  return await CountAsync(from);
}
```

# Getting rid of allocations

To even improve more, the `Task.FromResult(0)` is already completed, so it can be cached.

```csharp
private static Task<int> cached = Task.FromResult(0);

private Task<int> CountAsync(int from)
{
  if (from < 2)
  {
    return cached;
  }
  
  return await CountAsync(from);
}
```

But we could have more than one result, making this solution unthinkable.

# Using ValueTask

To remove the overhead of allocations, ValueTask is here to save the day.

```csharp
private ValueTask<int> CountAsync(int from)
{
  if (from < 2)
  {
    return new ValueTask<int>(44);
  }
  
  return new ValueTask<int>(await CountAsync(from));
}
```

The synchronous path will always allocate **0** bytes in this example.

# When to use `Task` or `ValueTask`

## Task

Can be awaited multiple times, by any number of consumers concurrently, with no problem.
Can be stored into a dictionary for any number of subsequent consumers to await in the future.
Can be blocked for waiting
Can write and consume large variety of combinators (WhenAll, WhenAny, ...)

Caching is use in the case of `Task<bool>`, `Task<int>`, Task of double (only 0 is cached) and for reference types, null is cached.

## ValueTask

Shouldn't be awaited multiple times. After the first await, the task goes back to the pool, and the next await could represent a totally different operation.
Shouldn't be awaited concurrently. Not thread-safe.
Shouldn't be blocked (`.Wait()`, `.GetAwaiter().GetResult()`). Could introduce strange behaviors.

## General rule about ValueTask

> With a `ValueTask` or `ValueTask<T>`, you should either await it directly (optionally with `.ConfigureAwait(false)`) or call `AsTask()` on it directly and then never use it again.
{.is-info}

# Differences in allocations

![valuetask_allocation_sample.png](/images/valuetask_allocation_sample.png)

# Await multiple times

> **Don't do it.**
Can cause unexcepted behaviors
> On .NET 5, this [will throw an exception](https://github.com/dotnet/coreclr/pull/26310#issue-309747841)
{.is-danger}
