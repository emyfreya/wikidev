---
title: Fire & forget
description: Short lesson about fire & forget techniques
published: 1
date: 2020-09-18T15:36:09.244Z
tags: 
editor: undefined
dateCreated: 2020-09-18T15:15:16.843Z
---

# async void

Using `async void` to launch an operation, obvisouly, we don't observe the result.
This can cause problems, because if we do not observe the result, it means **exceptions will not be catched**.

For **exceptions**, it uses `SynchronizationContext` to post exceptions. If `SynchronizationContext` is null, there is no way to handle it.

> This could lead to an application crash.
**Always try to avoid those methods as much as possible.**
{.is-danger}

> **Acceptable only** when it is used for an **event handler**, because of backwards compatibility.
{.is-success}

> It is possible to implement our own `SynchronizationContext` to handle `async void` exceptions.
[Implementations already exists](https://github.com/StephenCleary/AsyncEx/blob/e637035c775f99b50c458d4a90e330563ecfd07b/src/Nito.AsyncEx.Context/AsyncContext.cs) to handle exceptions properly.
{.is-info}

Also, it is not possible to unit test these methods. `async void` is anti-pattern for unit tests.

## How to avoid async void

Now that it returns a Task, we can try to use `Task.Run` with it, stealing a ThreadPool thread. Allowing exceptions to be catched.

Another solution is to use [Sync over async](/csharp/async/course/syncxasync).

> **Remember**: Could lead to thread starvation and / or deadlocking.
{.is-warning}

**Always better** to modify the code to return a `Task`.

## Getting the exception

We could use `async void` when we want to wrap the awaited `Task` and catch exceptions. 

Example [brminnick/AsyncAwaitBestPractices](https://github.com/brminnick/AsyncAwaitBestPractices).