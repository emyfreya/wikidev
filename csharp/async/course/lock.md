---
title: lock
description: 
published: 1
date: 2022-01-21T09:04:03.582Z
tags: 
editor: markdown
dateCreated: 2022-01-21T09:04:03.582Z
---

# Monitor

The API used behind the `lock` implementation.

Uses `Monitor.Enter` and `Monitor.Exit`.

`Monitor.TryEnter` has an argument with a TimeSpan to avoid deadlock.

# lock striping

Locking occurs on several buckets or stripes, meaning that accessing a bucket only locks that bucket and not the entire data structure.

# Exclusive locking

## Mutex

The use of `Mutex` can allow to synchronize Threads between processes. (Not only inside our application).

### Run only single instance

To start an application with a single instance by using a unique ID.

### Naming

By naming the mutex a certain way, the mutex will have a different scope

- `Local` prefix : Unique for each terminal session
- `Global` prefix : System wide unicity

Default is `Local`.

## SpinLock

A lightweight way of spinning in a loop, to check if a lock becomes available for example.

The goal is to take the least amount of performance just for a condition check.

# Non exclusive locking

More than one thread can use this synchronization.

For example: `Semaphore`, `SemaphoreSlim`.

- Can be used to control resource access, with a limit
- Has async await APIs
- No thread affinity (No owner)

# ReaderWriterLock

In a scenario in which there is either a lot of Reader or Writer.