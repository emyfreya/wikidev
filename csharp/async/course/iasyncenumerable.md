---
title: IAsyncEnumerable
description: 
published: true
date: 2021-11-30T13:51:41.043Z
tags: 
editor: markdown
dateCreated: 2021-11-30T09:18:49.309Z
---

# IEnumerable

To see why we would need an async version of that interface, we need to see what the following code is translated into :

```csharp
foreach (string line in File.ReadLines(path))
{
  if (MyFunction(line)) {
    // Do stuff
  }
}
```

is translated to 

```csharp
IEnumerable<string> lines = File.ReadLines(path);
using (IEnumerator<string> enumerator = lines.GetEnumerator())
{
  while (enumerator.MoveNext())
  {
    string line = enumerator.Current;
    
    if (MyFunction(line))
    {
      // Do stuff.
    }
  }
}
```

By design, `IEnumerator<T>` is `IDisposable`.
The goal here is what if the `.MoveNext()` would be asynchronous.

# IAsyncEnumerator

Since .NET Core 3, 3 new interfaces have been introduced :

- `IAsyncDisposable`
- `IAsyncEnumerable<T>`
- `IAsyncEnumerator<T>`

And since C# 8, the `foreach` and `using` keyword can be prefix with `await`.

For example :

```csharp
private IAsyncEnumerable<string> GetDataAsync() => // ... ;

// Code
await foreach (string line in GetDataAsync())
{
  // Do stuff
}
```

translated into :

```csharp
private IAsyncEnumerable<string> GetDataAsync() => // ... ;

// Code
await using (IEnumerator<string> enumerator = lines.GetEnumerator())
{
  while (await enumerator.MoveNextAsync())
  {
    string line = enumerator.Current;
  }
}
```

# Async Linq

The package [System.Linq.Async](https://www.nuget.org/packages/System.Linq.Async/) offers extensions to create the `IAsyncEnumerable<T>`.

## Example

```csharp
await foreach (string line in Enumerables.EnumerateLinesAsync(path)
  .WhereAwait(async x => await DoWorkAsync(x))
{
  // Do work
}
```

# Cancellation

- CancellationToken as a parameter to a mathod returning IAsyncEnumerable
- Use WithCancellation method on enumerator

## WithCancellation

The attribute `EnumeratorCancellationAttribute` allows the user of the method that returns an `IAsyncEnumerable<T>` to pass a `CancellationToken` via the generated method `WithCancellation`.

```csharp
await foreach (var i in GetDataAsync().WithCancellation(token))

public async IAsyncEnumerable GetDataAsync([EnumeratorCancellation] CancellationToken ct)
{
  // Do
}
```