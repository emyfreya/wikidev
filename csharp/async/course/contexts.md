---
title: Execution, contexts, scheduler
description: 
published: 1
date: 2020-09-11T14:56:31.933Z
tags: 
editor: undefined
dateCreated: 2020-09-10T16:54:30.597Z
---

# SynchronizationContext

## Functional view

Scheduling mechanism.

Knows how & where schedule a work item (like an async/await continuation).
May be different through all types of app & framework used (Console, UI, Web, Mobile).

## Technical view

Just an object.

Can be save and restore (Captured).
As like in the case of async/await state machine, used to remember how to schedule continuation.
Capture can be disabled with `ConfigureAwait`.

## Frameworks

The UI needs a custom SynchronizationContext, to schedule an operation to the associated thread related to the control (Also called the UI thread).

## ASP.NET

It implements its own SynchronizationContext, because it's required to execute awaits operations on the same thread which the http request has started.
This is called "http request base thread affinity". Every thread is responsible to handling a particular http request.
So it is needed to return to the same thread to get the data associated with http request.

Downside is it's needed to return to process the started request.
Scalibility is still there (Thread can still process multiple items) but limited.

## ASP.NET Core

There is no SynchronizationContext.
It uses TaskScheduler, allowing data to flow with the http request, allowing full scalability.

## Customization

SynchronizationContext can be customized via `SynchronizationContext.SetSynchronizationContext`.

# SynchronizationContext & .Result

Taking the following example:

```csharp
public async void NestedAsyncClick(object sender, EventArgs e)
{
  // Fine
  await GetAsync();  
}

public void NestedSyncClick(object sender, EventArgs e)
{
  // Deadlocks
  GetAsync().Result;
}

private async Task<string> GetAsync()
{
  using(var httpClient = new HttpClient())
  {
    return await httpClient.GetStringAsync();
  }
}
```

In `NestedSyncClick`, the `SynchronizationContext` is captured when `GetStringAsync` is called.
When we want to set the result, we're trying to set the continuation on the main UI thread (Because of the `SynchronizationContext`).

It deadlocks because:
The UI thread is blocked waiting for the task, and we're trying to continue the operation on the UI thread (Same thread) to set the task to completed.

> **TL;DR** we try to schedule an operation on the same waiting thread.
{.is-info}

In general, always avoid `.Result` because the `SynchronizationContext` could be implemented another way, and it may or may not deadlock.

If `async` is used all the way:
![csharp_synchronizationcontext_configureawait_asyncalltheway.png](/images/csharp_synchronizationcontext_asyncalltheway.png)

# ConfigureAwait

Returns a wrapper that needs to be awaited.

It is possible to ignore the `SynchronizationContext` by using `.ConfigureAwait(false)` on a `Task`.

The parameter simply means that we do not care about continuing on the original captured context.

Taking the previous example: If you add `.ConfigureAwait(false)` to `await httpClient.GetStringAsync()` it means the operation is scheduled to the **ThreadPool**, no continuation and no state machine involved for the original thread (UI thread in the example), allowing to unblock and complete the operation.

Using `.ConfigureAwait` with `.Result` can also avoid deadlocks:
![csharp_synchronizationcontex_configureawait_asyncalltheway.png](/images/csharp_synchronizationcontex_configureawait_asyncalltheway.png)

> `.Result` should never be used considering the risks.
{.is-danger}

> Every `await` is subject to a **context switch**, thus making library code obliged to add `.ConfigureAwait(false)` at every `await`.
{.is-info}

## Use cases

Shouldn't be used when writting app level code (UI related), could make things more complicated.

Should be used in librairies because the context isn't known.

# ContinueWith

It does not capture the `SynchronizationContext` by default. Better use async / await.

# TaskScheduler

If there is no `SynchronizationContext`, the `TaskScheduler` is used to queue work items.

`TaskScheduler.Default` uses the ThreadPool.
`TaskScheduler.FromCurrentSynchronizationContext()` creates a new task scheduler that **Post**s  tasks to the captured `SynchronizationContext`.

## ConcurrentExclusiveSchedulerPair

- ConcurrentScheduler
  'Reader', Many may run concurrently (Limit can be set).
- ExclusiveScheduler
  'Writer', only one can run (and no other concurrent).

These schedulers can be used for
- Read / Write locks
  Schedule reader tasks to `ConcurrentScheduler` and writer tasks to `ExclusiveScheduler`.
- Typical locks
  ExclusiveScheduler can be used on its own
- Limited concurrency
  Configurable via constructor
  
### Example

**Typical lock**

```csharp
var cesp = new ConcurrentExclusiveSchedulerPair();

Task.Factory.StartNew(() =>
{
	// Work
}, default, ,TaskCreationOptions.None, cesp.ExclusiveScheduler).Wait();
```

**Limited concurrency**

```csharp
var concurrent = new ConcurrentExclusiveSchedulerPair(TaskScheduler.Default, 8)
  .ConcurrentScheduler;
  
var exclusive = new ConcurrentExclusiveSchedulerPair(concurrent)
  .ExclusiveScheduler;
```

# ExecutionContext

A container to store data about the current code executed.
It flows with the asynchronous operations.

> It capture and restore information between threads
{.is-info}

Is build-in into async state machine builders