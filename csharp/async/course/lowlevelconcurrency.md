---
title: Low Level Concurrency
description: 
published: true
date: 2021-11-30T15:25:15.822Z
tags: 
editor: markdown
dateCreated: 2021-11-30T14:56:19.517Z
---

# Memory hierarchy

## Cache

- Hardware memory located closer to CPU
- Sotres copy of data from the main memory
- Separate caches for instructions and data
- Multiple levels for data cache (L1, L2, L3, ...)
  - L1 dedicated per core
  - Different sizes and optimizations for different level
- By default, all data read or written by the CPU cores is stored in the cache.

## Cache miss

The fact of accessing the cache, waiting for too long before continuing computation.

# CPU Pipeline

Operations :

- are **fetched** from memory
- are **decoded**
- require **reading** some data
- are **executed**
- might want to require writting

Those operations could be executed at the same time.

The pipeline can get stalled when the CPU needs data somewhere else.
Can be rolled back if a branch was mispredicted.
