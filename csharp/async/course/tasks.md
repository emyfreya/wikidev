---
title: Task, Task<T> & Async
description: Explanation of the usage of promises in C#
published: true
date: 2021-06-17T13:09:22.176Z
tags: 
editor: markdown
dateCreated: 2020-07-30T15:17:54.225Z
---

# Sources

- [StartNew is Dangerous](https://blog.stephencleary.com/2013/08/startnew-is-dangerous.html)
- [Dynamic Task Parallelism](https://docs.microsoft.com/en-us/previous-versions/msp-n-p/ff963551(v=pandp.10))
- [LongRunning Is Useless For Task.Run With Async/Await](http://blog.i3arnon.com/2015/07/02/task-run-long-running/)

# What is `Task` and `Task<T>`

This is a **promise** of the ongoing operation completion.

It could have 2 categories:
- Task executes code asynchronously
- Task representing events or signals (i.e. I/O operations)

Concept associated: *Task based Asynchronous Pattern (TAP)*

# Task API {.tabset}
## Task states

Not easily seen :

- Created, WaitingToRun : Not yet scheduled
- Running
- WaitingForChildrenToComplete : Waiting for attached child tasks

These important states each have their corresponding properties on `Task` :

- RanToCompletion : Successfuly finished
- Faulted (`Task.IsFaulted`) : Unhandled exception
- Canceled (`Task.IsCanceled`)

> `Task.IsCompleted` means that the task has finished successfully, has been canceled or has been faulted.
{.is-warning}

## Task.Delay

Registers a timer event that completes when the timer fires.

## Task.Yield

When awaited, transition back into the current SynchronizationContext at the time of the `await`.

Will queue the work item on the global task queue instead of the local task queue.

`Task.Yield` is used to make the method start in an asynchronous manner right away instead of waiting for the first "true" asynchronous await.

### Example

Useful in scenarios when you don't want the `async` method to block the caller's `SynchronizationContext`.

```csharp
private async void button_Click(object sender, EventArgs e)
{
  await Task.Yield(); 
  
  // Because the method is async, If Task.Yield wasn't there, it would block the UI.
  var data = DoThings();
  
  await UpdateDataAsync(data).ConfigureAwait(true);
}
```

## Task.Factory.StartNew

Immediatly schedules the delegate.

## Task.Run

Immediatly schedules the delegate.

Cannot use a custom **thread scheduler**.

## Task.Wait() / Task.Result

Blocks the thread to observe the task result.

Every `Task.Result` is **a blocking operation**.

## Task.Wait & Task.WaitAny (Obsolete)

Both of the methods wastes a thread by putting the thread it was running on the Waiting state.
It synchronously blocks the threads.

> Should be avoided in favor of `async`.
{.is-warning}

## Task.Unwrap

Gets the underlying `Task` passed in a lambda for example.

Could be used with `ContinueWith`. Always use it if you want to await the task put inside the lambda.

### Example

Initial demo
```csharp
Task<Task<string>> task = Task.Factory.StartNew(() => DoSomethingAsync("Test"));

var result = task.Result.Result;
```

Unwrap
```csharp
Task<string> task = Task.Factory.StartNew(() => DoSomethingAsync("Test"))
	.Unwrap();

var result = task.Result;
```

# Exception handling

`Task` contains an exception property, which is null when nothing happened.

If `Task.Result` is used, we can observe it with a `try { } catch`.

If the **faulted** task isn't observed via `Task.Result` or `Task.Wait()`:

- It will be observed by the GC when the task is collected
- Before .NET 4, it could kill the app
- In .NETCore and .NET Framework
	- Observable via `TaskScheduler.UnobservedTaskException` handler
	- Revert previous behavior via `COMPlus_ThrowUnobservedTaskExceptions` in .NET Core
  
# When to use `Task.Factory.StartNew`

Has more parameters to configure the future running task.
Defaults to `None` and `TaskScheduler.Current`.

Has problems with using async delegates (lambda).

`PreferFairness`: Global queue will be used instead of the local queue.
`Detached` task: by default.
`Child task`: Parent does not wait for it to be completed, does not propagate exceptions.

With Task.Run, default is `DenyChildAttach`. Those are complex and can introduce unexcepted behaviors.

## Example 

Will run both in "background":
```csharp
Task.Factory.StartNew(() => {
	// Child task
	Task.Factory.StartNew(() => { });
});
```

Will wait for the child task:
```csharp
Task.Factory.StartNew(() => {
	// Child task
	Task.Factory.StartNew(() => { },
  	TaskCreationOptions.AttachedToParent);
});
```

This is called **Dynamic Task Parallelism**. Should not be used.

# Task.Factory.StartNew and Tasks

Doing such code:

```csharp
var task = Task.Factory.StartNew(async () => await DoSleepAndEchoAsync("Hello world!", 1000));

await task;
```

Will print instantly the `.ToString` of the async delegate. 
To observe the result, unwrapping is **necessary**.
See `Task.Unwrap` section.

We could observe the result with this:
```csharp
Task<Task> task = Task.Factory.StartNew(async () => await DoSleepAndEchoAsync("Hello world!", 1000));

Task outerTask = await task;
string result = await outerTask;
```
or
```csharp
Task<Task> task = Task.Factory.StartNew(async () => await DoSleepAndEchoAsync("Hello world!", 1000));

string result = await await task;
```
or (Which is better and clearer)
```csharp
Task task = Task.Factory.StartNew(async () => await DoSleepAndEchoAsync("Hello world!", 1000))
  .Unwrap();

string result = await task;
```

# Long running tasks

Results of passing `TaskCreationOptions.LongRunning` to `Task.Factory.StartNew` **gives just a hint** of starting the task in a dedicated thread.

> If used with `Task.Run`, it could starve the thread pool because it consumes a new thread pool thread.
{.is-warning}

> `TaskCreationOptions.LongRunning` **is just a hint**
> It may run on the dedicated thread **until** the first await
{.is-warning}

If we want to be sure to run the long running task in a separate thread, use `Thread` API.

## Example

Here is an example of a code that will **not behave as expected**:

```csharp
public class ExampleClass
{
  private readonly BlockingCollection<string> _queue = new BlockingCollection<string>();
  
  public Task ProcessAsync()
  {
    return Task.Factory.StartNew(async () =>
    {
      foreach (var item in _queue.GetConsumingEnumerable())
      {
        // Will probably run the continuation on a threadpool thread.
        await ProcessItemAsync(item);
      }
    }, TaskCreationOptions.LongRunning)
    .Unwrap();
  }
}
```

It creates a thread, then will start or continue on another thread, making the thread creation useless.

# Task & lambda

With the following code:

```csharp
Task.Run(() => ... );
```

It creates a **closure**.

[Example gist here](https://gist.github.com/RamenTurismo/b03ff11d5d4517ede153c43629b93feb).

# Tabs {.tabset}
## Modified closure

```csharp
for(int i = 0; i < 10; i++)
{
	// WARNING: Access to modified closure.
	Task.Run(() => i);
}
```

## Solution

To fix this, we need to use a temporary variable.
```csharp
for(int i = 0; i < 10; i++)
{
	int temp = i;
	Task.Run(() => temp);
}
```

It has some overhead, because it creates a new reference for each iteration (More allocations).

## Solution with no overhead

```csharp
for(int i = 0; i < 10; i++)
{
	Task.Factory.StartNew(x => Console.WriteLine(x), i);
}
```

# Task chaining

Using `ContinueWith`. It means the next task will be run when the previous task is completed (`IsCompleted=true`).

- Supports `CancellationToken`, which is only for scheduling the continuation.
- ExecuteSynchronously means that the same thread that causes the previous task to transition into its final state is reused.
- `TaskScheduler.Current` is used by default.
- Often used for the **benefit** of having the previous task's result.
- Creates a new task with new allocations, compared to `async`.

This should be replaced by `async` & `await`.

## Exceptions

For example, we have 2 chained tasks, and we observe them with `Wait()`.
If the first task throws, and we try to catch the exception, it'll throw `AggregateException`, which contains an `AggregateException` were the InnerException contains the real exception.

# Task Cancellation

With `CancellationToken`, instead of silently close the task, we throw via `CancellationToken.ThrowIfCancellationRequested()`. It is prefered because it's the common way to handle cancellations.

If the token is passed to the Task (i.e. with `Task.Run`'s 2nd parameter), it cancels the **scheduling**, not the **operation** itself.