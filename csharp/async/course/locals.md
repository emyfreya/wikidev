---
title: Local
description: Threading locals
published: true
date: 2021-11-29T14:10:16.800Z
tags: 
editor: markdown
dateCreated: 2021-02-10T14:11:59.065Z
---

# Sources

* [Repository](https://gitlab.com/RamenTurismo/async-expert-course/-/tree/master/DemoApp/6%20-%20Locals)

# Thread local storage

Store data per thread with `[ThreadStatic]` attribute.

```csharp
[ThreadStatic]
public static string ThreadStatic;
```

> Avoid using **static** initializers, see below.
{.is-danger}

## Beware of static initializers : ThreadStaticAttribute and ThreadLocal

Taking the code below as an example:

```csharp
[ThreadStatic]
public static int? ThreadId = Thread.CurrentThread.ManagedThreadId;
```

The static initializer will set the value of the **first thread** that comes reading this line of code. Meaning that the first thread will initialize it to a value, and the rest to `default`.

> Tools like ReSharper raises a **warning**.
{.is-info}

Solution is to create a Lazy initialization :

```csharp
[ThreadStatic]
public static int? _threadId; 

public static int ThreadStatic => _threadId ??= Thread.CurrentThread.ManagedThreadId;
```

### ThreadLocal

`ThreadLocal<T>` is similar to `Lazy<T>`, but per **Thread**.

Or a better one is to use `ThreadLocal<T>` :

```csharp
// As a field
private ThreadLocal<int> _threadId = new ThreadLocal<int>(() => Thread.CurrentThread.ManagedThreadId);

// As a variable
public void MyMethod()
{
  using ThreadLocal<int> threadId = new ThreadLocal<int>(() => Thread.CurrentThread.ManagedThreadId);
  
  new Thread(() => Console.WriteLine(threadId.Value)).Start();
  new Thread(() => Console.WriteLine(threadId.Value)).Start();
}
```

> Accessing `ThreadLocal<T>` has more overread that the Attribute `ThreadStatic` because the implementation manages data structures.
{.is-info}

### ThreadLocal : TrackAllValues

The parameter `trackAllValues` can be set to `true` to get the property `Values` and the results of the threads even if the threads are gone.

This should not be used while the threads are running.

# Thread Safe Random

Calling `Random.Next()` with a shared instance across all threads is not thread safe. It could generate the same numbers.

Thus, the solution of making it Thread safe:

```csharp
private static readonly Random random = new Random();

[ThreadStatic]
private static Random local;

public static int Next()
{
  if(local is null)
  {
    int seed;
    
    lock(global)
    {
      seed = global.Next();
    }
    
    local = new Random(seed);
  }
  
  return local.Next();
}
```

Or an even faster one with `Interlocked` and `ThreadLocal` :

```csharp
private static int seed = 0;
private static readonly ThreadLocal<Random> random = 
    new ThreadLocal<Random>(Initialize);

public static Random Initialize()
{
  Interlocked.Increment(ref seed);
  return new Random(seed);
}

public static int Next() => local.Value.Next();
```

# AsyncLocal

Stores data related to the asynchronous operation (not the thread).
`ExecutionContext` uses `AsyncLocal<T>` to keep track of the caller's initial `ExecutionContext`.

Allows to control the flow of the async operation.