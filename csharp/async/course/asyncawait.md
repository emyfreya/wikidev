---
title: Async Await
description: Explanation of Async Await in C#
published: 1
date: 2020-08-11T15:58:18.176Z
tags: 
editor: undefined
dateCreated: 2020-08-04T11:42:08.205Z
---

# Documentation

[Asynchronous Programming - Async Performance: Understanding the Costs of Async and Await](https://docs.microsoft.com/en-us/archive/msdn-magazine/2011/october/asynchronous-programming-async-performance-understanding-the-costs-of-async-and-await)
[Async/Await FAQ](https://devblogs.microsoft.com/pfxteam/asyncawait-faq/)

# Async keyword

If the keyword `async` is used along with `Task`, the way the method is treated is different.

Allows to write **asynchronous** code code like **sequential** code.

## Example

```csharp
public int Method()
{
	return 0;
}

// Creates a state machine just because of the async keyword, and terminates synchronously.
public async Task<int> MethodAsync()
{
	return 0;
}
```

# Await

`await`ing is a way to wait for the result of a task without consuming a thread. Compared to the blocking way of waiting `.Wait()`.

![csharp_async_await.png](/images/csharp_async_await.png)

The thread can discontinue when the operation is ongoing. **No worker thread** are consumed while `await`ing.
It is not guaranted to return on the same thread when it queues back on the thread pool.

# Awaiting

Taking this example:

```csharp
async Task SomeAsync()
{
	await DoAsync();
  DoOther();
}
```

The method is split into parts:

```csharp
async Task SomeAsync()
{
	var task = DoAsync(); // Launch background operation
  // ------------------
  await task; // Await on the promise
  DoOther();
}
```

This parts are typically the result of awaiting on I/O bound operations.

The operation starts at the call of the method (`var task = DoAsync();`).

## Exceptions

`await` rethrow exceptions.

Validation exceptions (i.e. `ArgumentNullException`) and execution exceptions (i.e. File not accessible).

Recommandations by .NET founders.
Separation of validation and Task called.
StackTrace will be wrapped into the task.
```csharp
public Task DoSomethingAsync(int delay)
{
	if(delay < 0)
  	throw new ArgumentOutOfRangeException(nameof(delay));
  
  return DoSomethingInternalAsync(delay);
}
```

## Linq behavior

```csharp
static async Task SequentialAsync()
{
    var tasks = Enumerable.Range(0, 3)
        .Select(i =>
        {
            Console.WriteLine(i);

            return Task.Delay(1000);
        });

    foreach (Task task in tasks)
    {
        await task;
    }
}
```

The enumerable is not yet **materialised**. The current lambda passed is not executed.
Therefor, the duration of the method will be about **3** seconds.

# Async is for scalability

The below graph concludes that async is for **scalability**.

![csharp_async_sync_scalability.png](/images/csharp_async_sync_scalability.png)

Allows to await operations in a same worker and managing the IOCP on the same thread.

Having `async` `await` by default prepares for scability problems.
The performance overhead is produces should not be considered, as it is minimal. 

# Async/Await keyword behavior

`await` on `Task<T>` unwraps the task to get the result.

`await` checks if the operation has already completed. 
If is is not completed, it calls an async state machine that responds to a call of `MoveNext()` (Compiler generated code), and schedules a continuation on the current thread **synchronisation context** (With `Post`), if null, on the current `TaskScheduler`.

# Async & Cancellation

Guidelines states that every Async method should accept a `CancellationToken` and use it.

# await Task.Delay()

Thread free asynchronous wait operation. Does not block any thread.
Uses a timer that uses the thread-pool.

Used in "retry" scenarios.

# Async eliding

Means getting rid of the async / await keywords and just return the `Task`.

> Allows to **not** create a state machine just for the said task, and improves allocation and speed.
{.is-success}

> Exception handling: Can't catch execution errors, because the task has already started.
Behavior changes if the `Task` and `await` are separated.
Be aware of when to do it: With `using`, it shouldn't be done because we get out of the scope after returning the `Task`.
{.is-danger}

> Could be used with one lined code for **performance**.
{.is-info}

## Using case

```csharp
private async Task MyMethodAsync()
{ 
	using(var service = new Service())
  {
		return await service.GetAllAsync();
  }
}
```

> With eliding `service` will be disposed!
{.is-danger}

```csharp
private Task MyMethodAsync()
{ 
	using(var service = new Service())
  {
    return service.GetAllAsync();
  }
}
```

## Exception handling

```csharp
private async Task MyMethodAsync()
{ 
	try
  {
		return await _service.GetAllAsync();
  }catch(Exception e)
  {
  	_logger.Log...
  }
}
```

With eliding, the exceptions happening in `_service.GetAllAsync()` will not be catched.

```csharp
private Task MyMethodAsync()
{ 
	try
  {
		return _service.GetAllAsync();
  }catch(Exception e)
  {
  	_logger.Log...
  }
}
```

## Exemple

No eliding

```csharp
private async Task MyMethodAsync() => await _service.GetAllAsync();
```

Eliding

```csharp
private Task MyMethodAsync() => _service.GetAllAsync();
```