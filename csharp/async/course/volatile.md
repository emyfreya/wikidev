---
title: volatile
description: 
published: 1
date: 2022-01-21T09:04:03.582Z
tags: 
editor: markdown
dateCreated: 2022-01-21T09:04:03.582Z
---

# volatile keyword

Keyword that serves to do code plateform / hardware related.

- `Thread.VolatileRead()`
- `Thread.VolatileWrite()`

Could be used in old codebase. The old implementation uses FullMemoryBarrier.

[Sayonara volatile](http://joeduffyblog.com/2010/12/04/sayonara-volatile/)

The `volatile` keyword has acquire and release semantics.

## Compilation

- x86 arch : Reordered memory will not be taken into accoutn
- ARM7 : Weak memory model
- ARM8 : Weak memory model with different choice of barriers

## Example

```csharp
public class CancellationTokenSource : IDisposable
{
  private volatile int _state;

  // NotifyingState: int const 
  public bool IsCancellationRequested => _state >= NotifyingState;
}

public readonly struct CancellationToken
{
  public bool IsCancellationRequested => _souce != null && _source.IsCancellationRequested;
}

// Code
while (!token.IsCancellationRequested)
{
}
```